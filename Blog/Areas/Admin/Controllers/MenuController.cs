﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
        [ChildActionOnly]
        public ActionResult Index()
        {
            UserDao ud = new UserDao();
            User u = new User();
            u = ud.GetByLogin(User.Identity.Name);
            return View(u);
        }
    }
}