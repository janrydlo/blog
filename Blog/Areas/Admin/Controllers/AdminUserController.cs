﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    public class AdminUserController : Controller
    {
        // GET: Admin/AdminUser
        public ActionResult Index()
        {
            UserDao ud = new UserDao();
            ViewBag.Users = ud.GetAll();
          
            return View();
        }
        [Authorize(Roles = "admin")]
        public ActionResult Detail(int? id)
        {
            RoleDao rd = new RoleDao();
            ViewBag.Roles = rd.GetAll();
            UserDao ud = new UserDao();
            User u = new User();
            if (id != null)
            {
                u = ud.GetById((int)id);
            }
            return View(u);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult SaveUser(User u, int idRole)
        {
            if (ModelState.IsValid)
            {
                RoleDao rd = new RoleDao();
                Role r = rd.GetById(idRole);

                u.Role = r;

                UserDao ud = new UserDao();
                if (u.Id != 0)
                {
             
                    ud.Update(u);
                }
                else
                {
                    ud.Create(u);
                }

            }
            else
            {
                return View("Detail", u);
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int Id)
        {
            UserDao ud = new UserDao();
            User u = ud.GetById(Id);

            ud.Delete(u);

            TempData["message-success"] = "Uživatel " + u.Login + " byl úspěšně vymazán";
            return RedirectToAction("Index");
        }
    }
}