﻿using System;
using DataAccess.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using Blog.Utils;

namespace Blog.Controllers
{
    public class ArticleController : Controller
    {
        // GET: Article
        public ActionResult Index(int id)
        {
            CategoryDao cd = new CategoryDao();
            setCategories();
            setArchives();
            setTags();
            ViewBag.Comments = findComments(id);
            ViewBag.Rating = getRating(id);
            return View(findPost(id));
        }

        public RatingViewModel getRating(int id)
        {
            PostDao pd = new PostDao();
            Post p = new Post();
            p = pd.GetById(id);

            int text = 0;
            int picture = 0;
            int edu = 0;
            int overall = 0;
            int celkem = 0;
            foreach(Rating ra in p.Ratings)
            {
                text = text + ra.TextQuality;
                picture = picture + ra.PictureQuality;
                edu = edu + ra.EducationalQuality;
                overall = overall + ra.OverallQuality;
                celkem = celkem++;
            }

            if(celkem != 0)
            {
                text = text / celkem;
                picture = picture /celkem;
                edu = edu / celkem;
                overall = overall / celkem;
            }

            RatingViewModel r = new RatingViewModel();
            r.TextQuality = text;
            r.PictureQuality = picture;
            r.EducationalQuality = edu;
            r.OverallQuality = overall;
            return r;
        }

        public ActionResult addComment(string Author, string Content, int Id)
        {
            if (ModelState.IsValid)
            {
                UserDao ud = new UserDao();
                User u = ud.GetByLogin(User.Identity.Name);
                PostDao pd = new PostDao();
                Post p = pd.GetById(Id);
                Comment c = new Comment() { Content = Content, Date = DateTime.Now, Article = p, User = u };
                CommentDao cd = new CommentDao();
                cd.Create(c);

                return RedirectToAction("Index", new { id = Id });
            }else
            {
                return View("Index", new { id = Id });
            }
          
        }

        public ActionResult addRating(int text, int picture, int edu, int overall, int Id)
        {
            RatingDao rd = new RatingDao();
            Rating r = new Rating();
            r.TextQuality = text;
            r.PictureQuality = picture;
            r.EducationalQuality = edu;
            r.OverallQuality = overall;

            UserDao ud = new UserDao();
            User u = ud.GetByLogin(User.Identity.Name);
            r.Author = u;

            PostDao pd = new PostDao();
            Post p = new Post();
            p = pd.GetById(Id);

            r.Article = p;

            rd.Create(r);

            return RedirectToAction("Index", new { id = Id });

        }

        private void setArchives()
        {
            PostDao pd = new PostDao();
            List<DateTime> dts = pd.getAllDates();
            ViewBag.Dates = DateUtils.getMonthNames(dts[0], dts[dts.Count - 1]);
        }

        private void setCategories()
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();
        }

        private Post findPost(int id)
        {
            PostDao pd = new PostDao();
            Post p = pd.GetById(id);
            return p;
        }
        
        private List<Comment> findComments(int id)
        {
            CommentDao cd = new CommentDao();

            return cd.getAllForPost(id);
        }
        private void setTags()
        {
            TagDao td = new TagDao();
            ViewBag.Tags = td.findAllActive();
        }
    }
}