﻿using DataAccess.Model;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Dao
{
    public class TagDao : DaoBase<Tag>
    {
        public TagDao() : base()
        {

        }

        public List<String> getTagNames(String query)
        {
            return (List<String>)session.CreateCriteria<Tag>()
              .Add(Restrictions.Like("Name", String.Format("%{0}%", query)))
              .SetProjection(Projections.Property("Name"))
              .List<String>();
        }

        public Tag findByName(String name)
        {
            return session.CreateCriteria<Tag>()
                .Add(Restrictions.Eq("Name", name))
                .UniqueResult<Tag>();
        }

        public List<Tag> findAllActive()
        {
            return (List<Tag>)session.CreateCriteria<Tag>()
                .Add(Restrictions.IsNotEmpty("Posts"))
                .SetMaxResults(10)
                .List<Tag>();
        }
    }
}
