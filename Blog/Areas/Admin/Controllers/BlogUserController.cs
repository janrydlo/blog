﻿using Blog.Utils;
using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin, reader")]
    public class BlogUserController : Controller
    {
        // GET: Admin/User
        public ActionResult Index()
        {
            UserDao ud = new UserDao();
            User u = ud.GetByLogin(User.Identity.Name);
            return View(u);
        }

        [HttpPost]
        public ActionResult SaveUser(User user, HttpPostedFileBase picture)
        {
         
                if (picture != null)
                {
                    if (picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {
                        Image image = Image.FromStream(picture.InputStream);
                        Bitmap small = new Bitmap(ImageUtils.ScaleImage(image, 350, 350));

                        if (user.ProfileImage != null)
                        {

                            System.IO.File.Delete(Server.MapPath("~/Upload/UserImages/" + user.ProfileImage));
                        }

                        user.ProfileImage = saveImage(small);

                        image.Dispose();
                        small.Dispose();
                    }

                  
                }
                UserDao ud = new UserDao();

                ud.Update(user);
 
            return RedirectToAction("Index", "Home");
        }

        private string saveImage(Bitmap b)
        {
            Guid guid = Guid.NewGuid();
            string imageName = guid.ToString() + ".jpg";
            b.Save(Server.MapPath("~/Upload/UserImages/" + imageName), ImageFormat.Jpeg);
            return imageName;
        }

    }

}
