﻿using DataAccess.Model;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Dao
{
    public class CommentDao : DaoBase<Comment>
    {
        public CommentDao() : base()
        {

        }

        public List<Comment> getAllForPost(int Id)
        {
            return (List<Comment>)session.CreateCriteria<Comment>().Add(Restrictions.Eq("Article.Id", Id)).List<Comment>();
        }
    }
}
