﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Utils
{
    public static class CollectioUtils
    {
        public static bool IsAny<T>(this IEnumerable<T> data)
        {
            return data != null && data.Any();
        }

    }

}