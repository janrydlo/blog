﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Model;
using System.Drawing;
using Blog.Utils;
using System.Drawing.Imaging;
using DataAccess.Dao;

namespace Blog.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();

            PostDao pd = new PostDao();
            return View(pd.GetAll());
        }

        public ActionResult Detail(int? id)
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();
            Post p = new Post();
            if(id != null)
            {
                PostDao pd = new PostDao();
                p = pd.GetById((int)id);
            }
            return View(p);
        }

        [Authorize(Roles = "admin")]
        public ActionResult DetailCategory(int? id)
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();
            Category c = new Category();
            if(id != null)
            {
                c = cd.GetById((int)id);
            }
            return View(c);
        }

        [HttpPost]
        public ActionResult Save(Post p, HttpPostedFileBase picture, int categoryId)
        {
            if (ModelState.IsValid)
            {
                if(picture != null)
                {
                    if(picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {
                        Image image = Image.FromStream(picture.InputStream);
                        Bitmap small = new Bitmap(ImageUtils.ScaleImage(image, 350, 232));
                        Bitmap big =  new Bitmap(ImageUtils.ScaleImage(image, 1000, 664));

                        if(p.PreviewImageName != null)
                        {
                            System.IO.File.Delete(Server.MapPath("~/Upload/Small/" + p.PreviewImageName));
                        }
                        if(p.ContentImageName != null)
                        {
                            System.IO.File.Delete(Server.MapPath("~/Upload/" + p.ContentImageName));
                        }

                        p.PreviewImageName = saveImage(small, true);
                        p.ContentImageName = saveImage(big, false);

                        image.Dispose();
                        small.Dispose();
                        big.Dispose();
                    }
                    p.CreationDate = DateTime.Now;
                    CategoryDao cd = new CategoryDao();
                    Category c = cd.GetById(categoryId);
                    p.Category = c;

                    UserDao ud = new UserDao();
                    User u = ud.GetById(1);
                    p.Author = u;

                    PostDao pd = new PostDao();
                   if(p.Id != 0)
                    {
                        pd.Update(p);
                    }else
                    {
                        pd.Create(p);
                    }
                }
            }else
            {
                return View("Detail", p);
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult SaveCategory(Category c)
        {
            if (ModelState.IsValid)
            {
                CategoryDao cd = new CategoryDao();
                if (c.Id != 0)
                {
                    cd.Update(c);
                }
                else
                {
                    cd.Create(c);
                }
 
            }
            else
            {
                return View("DetailCategory", c);
            }
            return RedirectToAction("Index");
        }


        private string saveImage(Bitmap b, Boolean small)
        {
            if (small)
            {
                Guid guid = Guid.NewGuid();
                string imageName = guid.ToString() + "_small"+".jpg";
                b.Save(Server.MapPath("~/Upload/Small/" + imageName), ImageFormat.Jpeg);
                return imageName;
            }
            else
            {
                Guid guid = Guid.NewGuid();
                string imageName = guid.ToString() + ".jpg";
                b.Save(Server.MapPath("~/Upload/" + imageName), ImageFormat.Jpeg);
                return imageName;
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeletePost(int Id)
        {
            PostDao pd = new PostDao();
            Post p = pd.GetById(Id);

            System.IO.File.Delete(Server.MapPath("~/Upload/" + p.ContentImageName));
            System.IO.File.Delete(Server.MapPath("~/Upload/Small/" + p.PreviewImageName));

            CommentDao cd = new CommentDao();
            List<Comment> comments = cd.getAllForPost(Id);
            foreach(Comment c in comments)
            {
                cd.Delete(c);
            }

            pd.Delete(p);

            TempData["message-success"] = "Příspěvek " + p.Title + " byl úspěšně vymazán";
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeleteCategory(int Id)
        {
            CategoryDao cd = new CategoryDao();
            Category c = cd.GetById(Id);

            cd.Delete(c);

            TempData["message-success"] = "Kategorie " + c.Name + " byla úspěšně vymazána";
            return RedirectToAction("Index");
        }

    }
}