﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Post : IEntity
    {
        public virtual int Id{ get; set; }

        [Required(ErrorMessage = "Každý článek musí mít titulek")]
        [MaxLength(250, ErrorMessage ="Nadpis může mít maximálně 250 znaků")]
        public virtual string Title { get; set; }
        [Required(ErrorMessage = "Každý článek musí mít anotaci")]
        [MaxLength(500, ErrorMessage = "Anotace může mít maximálně 250 znaků")]
        public virtual string Annotation { get; set; }
        [Required(ErrorMessage = "Každý článek musí mít obsah")]
        [AllowHtml]
        public virtual string Content { get; set; }
        public virtual User Author { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual string PreviewImageName { get; set; }
        public virtual string ContentImageName { get; set; }
        public virtual Category Category { get; set; }

        public virtual IList<Tag> Tags { get; set; }
        public virtual IList<BlogImage>Gallery { get; set; }
        public virtual IList<Rating> Ratings { get; set; }
    }
}
