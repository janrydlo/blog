﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace DataAccess.Model
{
    public class User : MembershipUser, IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Login { get; set; }
        public virtual string Password { get; set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual Role Role { get; set; }
        public virtual string ProfileImage { get; set; }
        [AllowHtml]
        public virtual string About { get; set; }

    }
}
