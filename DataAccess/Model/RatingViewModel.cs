﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class RatingViewModel
    {
        public int TextQuality { get; set; }
        public int PictureQuality { get; set; }
        public int EducationalQuality { get; set; }
        public int OverallQuality { get; set; }
    }
}
