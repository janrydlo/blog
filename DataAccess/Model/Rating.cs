﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class Rating : IEntity
    {
        public virtual int Id { get; set; }
        public virtual int TextQuality { get; set; }
        public virtual int PictureQuality { get; set; }
        public virtual int EducationalQuality { get; set; }
        public virtual int OverallQuality { get; set; }
        public virtual Post Article { get; set; }
        public virtual User Author { get; set; }
    }
}
