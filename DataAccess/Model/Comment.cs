﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class Comment : IEntity
    {
        public virtual int Id { get; set; }
        [Required(ErrorMessage = "Je potřeba vyplnit obsah komentáře")]
        public virtual string Content { get; set; }
        public virtual User User { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual Post Article { get; set; }
    }
}
