﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class CommentController : Controller
    {   
        public ActionResult Index(int id)
        {
            CommentDao cd = new CommentDao();
            
            return View(cd.getAllForPost(id));
        }

        public ActionResult Detail(int id)
        {
            CommentDao cd = new CommentDao();
            Comment c = cd.GetById(id);
            return View(c);
        }
        [HttpPost]
        public ActionResult Save(Comment c)
        {
            if (ModelState.IsValid)
            {
                CommentDao cd = new CommentDao();
                cd.Update(c);
                return RedirectToAction("Index", "Comment", new { id = c.Article.Id });
            }else
            {
                return View("Detail", c);
            }
           
        }

        public ActionResult DeleteComment(int id)
        {
            CommentDao cd = new CommentDao();
            Comment c = cd.GetById(id);
            int idClanek = c.Article.Id;
            cd.Delete(c);
            return RedirectToAction("Index", "Comment", new { id = idClanek });
        }
    }
}