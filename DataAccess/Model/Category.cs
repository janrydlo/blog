﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class Category : IEntity
    {
        public virtual int Id { get; set; }
        [Required(ErrorMessage = "Je potřeba vyplnit jméno")]
        [MaxLength(50, ErrorMessage = "Jméno kategorie je moc dlouhé (max.50 znaků)")]
        public virtual String Name { get; set; }
        [Required(ErrorMessage = "Je potřeba vyplnit popisek")]
        [MaxLength(200, ErrorMessage = "Popisek kategorie je moc dlouhý (max.200 znaků)")]
        public virtual String Description { get; set; }
        public virtual Boolean Active { get; set; }

    }
}
