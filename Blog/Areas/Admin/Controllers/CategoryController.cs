﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Admin/Category
        public ActionResult Index()
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult DetailCategory(int? id)
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();
            Category c = new Category();
            if (id != null)
            {
                c = cd.GetById((int)id);
            }
            return View(c);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult SaveCategory(Category c)
        {
            if (ModelState.IsValid)
            {
                CategoryDao cd = new CategoryDao();
                if (c.Id != 0)
                {
                    cd.Update(c);
                }
                else
                {
                    cd.Create(c);
                }

            }
            else
            {
                return View("DetailCategory", c);
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeleteCategory(int Id)
        {
            CategoryDao cd = new CategoryDao();
            Category c = cd.GetById(Id);

            cd.Delete(c);

            TempData["message-success"] = "Kategorie " + c.Name + " byla úspěšně vymazána";
            return RedirectToAction("Index");
        }
    }
}