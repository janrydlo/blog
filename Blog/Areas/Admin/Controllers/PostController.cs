﻿using Blog.Utils;
using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin, reader")]
    public class PostController : Controller
    {
        int itemsOnPage = 1;
        int totalPosts;
        // GET: Admin/Post
        public ActionResult Index(int? idUser, int? page, String phrase)
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();

            int pg = page.HasValue ? page.Value : 1;

            UserDao ud = new UserDao();
            User u = ud.GetByLogin(User.Identity.Name);

            PostDao pd = new PostDao();
            if (u.Role.Identificator == "admin")
            {
                IList<Post> articles = pd.getPostsByTitle(phrase, itemsOnPage, (int)pg, out totalPosts);
                ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
                ViewBag.CurrentPage = pg;
                ViewBag.Phrase = phrase;
                if (Request.IsAjaxRequest())
                {
                    return PartialView(articles);
                }
                return View(articles);
            }else
            {
                IList<Post> articles = pd.getByUser((int)u.Id, phrase, itemsOnPage, (int)pg, out totalPosts);
                ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
                ViewBag.CurrentPage = pg;
                if (Request.IsAjaxRequest())
                {
                    return PartialView(articles);
                }
                return View(articles);
            }
        }

        public ActionResult Detail(int? id)
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();
            Post p = new Post();
            if (id != null)
            {
                PostDao pd = new PostDao();
                p = pd.GetById((int)id);
            }
           
            return View(p);
        }


        [HttpPost]
        public ActionResult Save(Post p, HttpPostedFileBase picture, int categoryId, String tag, IEnumerable<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {

                if (picture != null)
                {
                    if (picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {
                        System.Drawing.Image image = Image.FromStream(picture.InputStream);
                        Bitmap small = new Bitmap(ImageUtils.ScaleImage(image, 350, 232));
                        Bitmap big = new Bitmap(ImageUtils.ScaleImage(image, 1000, 664));

                        if (p.PreviewImageName != null)
                        {
                            System.IO.File.Delete(Server.MapPath("~/Upload/Small/" + p.PreviewImageName));
                        }
                        if (p.ContentImageName != null)
                        {
                            System.IO.File.Delete(Server.MapPath("~/Upload/" + p.ContentImageName));
                        }

                        p.PreviewImageName = saveImage(small, true);
                        p.ContentImageName = saveImage(big, false);

                        image.Dispose();
                        small.Dispose();
                        big.Dispose();
                    }
                }

                if (!String.IsNullOrEmpty(tag))
                {
                    p.Tags = saveTags(tag);
                }
                p.CreationDate = DateTime.Now;


                CategoryDao cd = new CategoryDao();
                Category c = cd.GetById(categoryId);
                p.Category = c;

                UserDao ud = new UserDao();
                User u = ud.GetByLogin(User.Identity.Name);
                p.Author = u;

               

                PostDao pd = new PostDao();
                if (p.Id != 0)
                {
                    pd.Update(p);
                }
                else
                {
                    pd.Create(p);
                }

                List<BlogImage> results = new List<BlogImage>();
                if (files.Count() != 1)
                {

                    BlogImageDao bid = new BlogImageDao();
                    List<BlogImage> images = bid.findByArticle(p.Id);
                    foreach (BlogImage blogi in images)
                    {
                        System.IO.File.Delete(Server.MapPath("~/Upload/Gallery/" + blogi.Name + blogi.Postfix));
                        System.IO.File.Delete(Server.MapPath("~/Upload/Gallery/Small/" + blogi.Name + "small" + blogi.Postfix));
                        bid.Delete(blogi);
                    }

                    foreach (HttpPostedFileBase hpfb in files)
                    {
                        
                        BlogImage bi = new BlogImage();
                        Guid guid = Guid.NewGuid();
                        bi.Name = guid.ToString();
                        bi.Postfix = ".jpg";
                        bi.Folder = "Gallery";
                        bi.Article = p;
                        bid.Create(bi);
                        results.Add(bi);


                        Bitmap b = new Bitmap(Image.FromStream(hpfb.InputStream));
                        Bitmap bs = new Bitmap(ImageUtils.ScaleImage(b,200,200));
                        b.Save(Server.MapPath("~/Upload/Gallery/" + bi.Name + bi.Postfix), ImageFormat.Jpeg);
                        bs.Save(Server.MapPath("~/Upload/Gallery/Small/" + bi.Name + "small" + bi.Postfix), ImageFormat.Jpeg);
                        b.Dispose();
                        bs.Dispose();
                    }
                }
                


            }
            else
            {
                CategoryDao cd = new CategoryDao();
                ViewBag.Categories = cd.GetAll();
                return View("Detail", p);
            }
            return RedirectToAction("Index");
        }

        private List<Tag> saveTags(String tag)
        {
            string[] tokens = tag.Split(',');

            List<Tag> tags = new List<Tag>();
            TagDao td = new TagDao();

            for(int i = 0; i < tokens.Length; i++)
            {
                    Tag t;
                    t = new Tag();
                    t.Name = tokens[i];

                    td.Create(t);
               
                
                tags.Add(t);
                }
           
            return tags;
        }
 


        private string saveImage(Bitmap b, Boolean small)
        {
            if (small)
            {
                Guid guid = Guid.NewGuid();
                string imageName = guid.ToString() + "_small" + ".jpg";
                b.Save(Server.MapPath("~/Upload/Small/" + imageName), ImageFormat.Jpeg);
                return imageName;
            }
            else
            {
                Guid guid = Guid.NewGuid();
                string imageName = guid.ToString() + ".jpg";
                b.Save(Server.MapPath("~/Upload/" + imageName), ImageFormat.Jpeg);
                return imageName;
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeletePost(int Id)
        {
            PostDao pd = new PostDao();
            Post p = pd.GetById(Id);

            System.IO.File.Delete(Server.MapPath("~/Upload/" + p.ContentImageName));
            System.IO.File.Delete(Server.MapPath("~/Upload/Small/" + p.PreviewImageName));

            CommentDao cd = new CommentDao();
            List<Comment> comments = cd.getAllForPost(Id);
            foreach (Comment c in comments)
            {
                cd.Delete(c);
            }

            pd.Delete(p);

            TempData["message-success"] = "Příspěvek " + p.Title + " byl úspěšně vymazán";
            return RedirectToAction("Index");
        }

        public JsonResult SearchTags(string query)
        {
            TagDao td = new TagDao();
            List<String> tags= td.getTagNames(query);
            return Json(tags, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PhotoEdit(int idPost)
        {
            BlogImageDao bid = new BlogImageDao();
            return View();
        }

    }
}
