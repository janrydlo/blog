﻿using DataAccess.Model;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Dao
{
    public class BlogImageDao : DaoBase<BlogImage>
    {
        public BlogImageDao() : base()
        {

        }

        public List<BlogImage> findByArticle(int id)
        {
            return (List<BlogImage>)session.CreateCriteria<BlogImage>()
                .CreateAlias("Article", "a")
                .Add(Restrictions.Eq("a.Id", id))
                .List<BlogImage>();
         }
    }
}
