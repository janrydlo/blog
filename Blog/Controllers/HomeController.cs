﻿using Blog.Utils;
using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        int itemsOnPage = 5;
        int totalPosts;

        public ActionResult Index(int? page)
        {
            int pg = page.HasValue ? page.Value : 1;
            IList<Post> articles = getArticles(pg);
            ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;
            setTags();
            setCategories();
            setArchives();
            if (Request.IsAjaxRequest())
            {
                return PartialView(getArticles(pg));
            }else
            {
                return View(getArticles(pg));
            }
        }

        public ActionResult getAjaxContentByTitle(string title, int? page)
        {
            setCategories();
            setArchives();
            setTags();
            int pg = page.HasValue ? page.Value : 1;
            PostDao pd = new PostDao();
            IList<Post> articles = pd.getPostsByTitle(title, itemsOnPage, (int)pg, out totalPosts);
            ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;
            ViewBag.Title = title;
            return View(articles);
        }

        public ActionResult GetAjaxContent(int id, int? page, bool? detail)
        {
            setCategories();
            setArchives();
            setTags();
            int pg = page.HasValue ? page.Value : 1;
            PostDao pd = new PostDao();
            IList<Post> articles = pd.getByCategory(id, itemsOnPage, (int)pg, out totalPosts);
            ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;
            CategoryDao cd = new CategoryDao();
            ViewBag.Category = cd.GetById(id);
            ViewBag.id = id;
            if(detail != null)
            {
                return View(articles);
            }
            return PartialView(articles);
        }


        public ActionResult GetAjaxContentByDate(DateTime date, int? page, bool? detail)
        {
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            setCategories();
            setArchives();
            setTags();
            int pg = page.HasValue ? page.Value : 1;
            PostDao pd = new PostDao();
            IList<Post> articles = pd.getPostsByDate(firstDayOfMonth, lastDayOfMonth, itemsOnPage, (int)pg, out totalPosts);
            ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;
        

            ViewBag.Date = date;
            if(detail != null)
            {
                return View(articles);
            }
            return PartialView(articles);
        }

        public ActionResult GetAjaxUsersPosts(int? page, bool? detail)
        {
            int pg = page.HasValue ? page.Value : 1;
            PostDao pd = new PostDao();
            IList<Post> article = pd.getUsersPosts(itemsOnPage, (int)pg, out totalPosts);
            ViewBag.CurrentPage = pg;
            ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
            if (detail != null)
            {
                return View(article);
            }
            return PartialView(article);
        }

        public ActionResult GetAjaxPostByTag(int? page, int idTag, bool? detail)
        {
            setCategories();
            setArchives();
            setTags();
            int pg = page.HasValue ? page.Value : 1;
            PostDao pd = new PostDao();
            IList<Post> articles = pd.getPostsByTag(idTag, itemsOnPage, (int)pg, out totalPosts);
            ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;
 
            ViewBag.id = idTag;
            if(detail != null)
            {
                return View(articles);
            }
            return PartialView(articles);
        }

        public ActionResult getAjaxContentUserDetail(int userId)
        {
            UserDao ud = new UserDao();
            User u = ud.GetById(userId);
            return PartialView(u);
        }

        private IList<Post> getArticles(int? page)
        {
            IList<Post> articles;
            PostDao pd = new PostDao();
            articles = pd.getPostsPaged(itemsOnPage, (int)page, out totalPosts);
            return articles;
       
        }
        private void setCategories()
        {
            CategoryDao cd = new CategoryDao();
            ViewBag.Categories = cd.GetAll();
        }

        private void setTags()
        {
            TagDao td = new TagDao();
            ViewBag.Tags = td.findAllActive();
        }

        private void setArchives()
        {
            PostDao pd = new PostDao();
            List<DateTime> dts = pd.getAllDates();
            ViewBag.Dates = DateUtils.getMonthNames(dts[0], dts[dts.Count - 1]);
        }

       
    }
}