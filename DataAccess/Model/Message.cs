﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class Message : IEntity
    {
        public virtual int Id { get; set; }
        [Required(ErrorMessage = "Pokud chcete zanechat zprávu, vyplňte email")]
        [MaxLength(50, ErrorMessage = "EMail je moc dlouhý")]
        public virtual string Email { get; set; }
        [Required(ErrorMessage = "Pokud chcete zanechat zprávu, vyplnte její obsah")]
        public virtual string Content { get; set; }
        public virtual bool Completed { get; set; }
        public virtual DateTime CreationDate { get; set; }
    }
}
