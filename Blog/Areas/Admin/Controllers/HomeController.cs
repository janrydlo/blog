﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            // User u = new UserDao().GetByLogin(User.Identity.Name);
            UserDao ud = new UserDao();
            PostDao pd = new PostDao();
            CommentDao cd = new CommentDao();
            Statistics s = new Statistics();
            BlogImageDao bid = new BlogImageDao();
            CategoryDao cad = new CategoryDao();

            TagDao td = new TagDao();
            s.UserCount = ud.Count();
            s.ArticleCount = pd.Count();
            s.CommentCount = cd.Count();
            s.ImageCount = bid.Count();
            s.TagCount = td.Count();
            s.CategoryCount = cad.Count(); 
            return View(s);
        }
    }
}