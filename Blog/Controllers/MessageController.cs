﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class MessageController : Controller
    {
        // GET: Message
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }else
            {
                return View();
            }
           
        }

        public ActionResult Save(Message m)
        {
            if (ModelState.IsValid)
            {
                m.Completed = false;
                m.CreationDate = DateTime.Now;

                MessageDao md = new MessageDao();
                md.Create(m);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Index", m);
            }
        }
    }
}