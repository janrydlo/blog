﻿using DataAccess.Model;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Dao
{
    public class MessageDao : DaoBase<Message>
    {
        public MessageDao() : base()
        {

        }

        public List<Message> getMessageByEmail(string email, bool neprectene, int count, int page, out int totalMessages)
        {
            if (neprectene)
            {
                totalMessages = session.CreateCriteria<Message>()
               .Add(Restrictions.Like("Email", String.Format("%{0}%", email)))
               .Add(Restrictions.Eq("Completed", false))
               .SetProjection(Projections.RowCount())
               .UniqueResult<int>();

                return (List<Message>)session.CreateCriteria<Message>().
               Add(Restrictions.Like("Email", String.Format("%{0}%", email)))
               .Add(Restrictions.Eq("Completed", false))
               .AddOrder(Order.Asc("CreationDate"))
               .SetFirstResult((page - 1) * count)
               .SetMaxResults(count)
               .List<Message>();
            }
            else
            {
                totalMessages = session.CreateCriteria<Message>()
              .Add(Restrictions.Like("Email", String.Format("%{0}%", email)))
              .SetProjection(Projections.RowCount())
              .UniqueResult<int>();

                return (List<Message>)session.CreateCriteria<Message>().
               Add(Restrictions.Like("Email", String.Format("%{0}%", email)))
               .AddOrder(Order.Asc("CreationDate"))
               .SetFirstResult((page - 1) * count)
               .SetMaxResults(count)
               .List<Message>();
            }
          
        }
    }
}
