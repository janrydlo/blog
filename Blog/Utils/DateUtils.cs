﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Utils
{
    public class DateUtils
    {
        public static List<DateTime> getMonthNames(DateTime from, DateTime to)
        {
            List<DateTime> results = new List<DateTime>();
            while (from < to)
            {
                results.Add(from);
                from = from.AddMonths(1);
            }
            return results;
        }
    }
}

