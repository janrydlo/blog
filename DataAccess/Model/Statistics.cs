﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class Statistics
    {
        public int ArticleCount { get; set; }
        public int CommentCount { get; set; }
        public int TagCount { get; set; }
        public int UserCount { get; set; }
        public int ImageCount { get; set; }
        public int CategoryCount { get; set; }
    }
}
