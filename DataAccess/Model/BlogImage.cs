﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class BlogImage : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Postfix { get; set; }
        public virtual string Folder { get; set; }
        public virtual Post Article { get; set; }
    }
}
