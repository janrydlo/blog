﻿using DataAccess.Model;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Dao
{
    public class PostDao : DaoBase<Post>
    {
        public PostDao() : base()
        {

        }
       

        public List<Post> getByCategory(int idCategory, int count, int page, out int totalPosts)
        {
            totalPosts = session.CreateCriteria<Post>().Add(Restrictions.Eq("Category.Id", idCategory)).SetProjection(Projections.RowCount()).UniqueResult<int>();
            return (List<Post>)session.CreateCriteria<Post>()
                .Add(Restrictions.Eq("Category.Id", idCategory))
                .AddOrder(Order.Desc("CreationDate"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count).List<Post>();
        }

        public List<Post> getByUser(int idUser, string title, int count, int page, out int totalPosts)
        {
            totalPosts = session.CreateCriteria<Post>()
                 .Add(Restrictions.Eq("Author.Id", idUser))
                 .Add(Restrictions.Like("Title", String.Format("%{0}%", title)))
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return (List<Post>)session.CreateCriteria<Post>()
                .Add(Restrictions.Eq("Author.Id", idUser))
                .Add(Restrictions.Like("Title", String.Format("%{0}%", title)))
                .List<Post>();
        }

        public List<Post> getPostsPaged(int count, int page, out int totalPosts)
        {
            totalPosts = session.CreateCriteria<Post>().SetProjection(Projections.RowCount()).UniqueResult<int>();

            return (List<Post>)session.CreateCriteria<Post>().AddOrder(Order.Asc("CreationDate")).SetFirstResult((page - 1) * count).SetMaxResults(count).List<Post>();
        }

        public List<Post> getPostsByTitle(string title, int count, int page, out int totalPosts)
        {
            totalPosts = session.CreateCriteria<Post>()
                .Add(Restrictions.Like("Title", String.Format("%{0}%", title)))
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return (List<Post>) session.CreateCriteria<Post>().
                Add(Restrictions.Like("Title", String.Format("%{0}%", title)))
                .AddOrder(Order.Desc("CreationDate"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<Post>();
        }

        public List<Post> getPostsByDate(DateTime from, DateTime to, int count, int page, out int totalPosts)
        {
            totalPosts = session.CreateCriteria<Post>()
                .Add(Restrictions.Le("CreationDate", to))
                .Add(Restrictions.Ge("CreationDate", from))
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return (List<Post>)session.CreateCriteria<Post>()
                .Add(Restrictions.Le("CreationDate", to))
                .Add(Restrictions.Ge("CreationDate", from))
                .AddOrder(Order.Asc("CreationDate"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<Post>();
        }

        public List<Post> getPostsByTag(int idTag, int count, int page, out int totalPosts)
        {
            totalPosts = session.CreateCriteria<Post>()
                .CreateAlias("Tags", "tags")
                .Add(Restrictions.Eq("tags.Id", idTag))
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return (List<Post>)session.CreateCriteria<Post>()
                .CreateAlias("Tags", "tags")
                 .Add(Restrictions.Eq("tags.Id", idTag))
                .AddOrder(Order.Desc("CreationDate"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<Post>();

        }

        public List<DateTime> getAllDates()
        {
            return (List<DateTime>) session.CreateCriteria<Post>()
                    .SetProjection(Projections.Property("CreationDate"))
                    .AddOrder(Order.Asc("CreationDate"))
                    .List<DateTime>();
        }

        public List<Post> getUsersPosts(int count, int page, out int totalPosts)
        {
            totalPosts = session.CreateCriteria<Post>()
                .CreateAlias("Author", "a")
               .Add(Restrictions.Not(Restrictions.Eq("a.Role.Id", 1)))
               .SetProjection(Projections.RowCount())
               .UniqueResult<int>();

            return (List<Post>)session.CreateCriteria<Post>()
                 .CreateAlias("Author", "a")
                .Add(Restrictions.Not(Restrictions.Eq("a.Role.Id", 1)))
                .AddOrder(Order.Desc("CreationDate"))
                 .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<Post>();
        }
    }
}
