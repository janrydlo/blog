﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    public class MessageController : Controller
    {
        int itemsOnPage = 1;
        int totalPosts;
        // GET: Admin/Message
        public ActionResult Index(int? page, String phrase, bool? neprectene)
        {
            if(neprectene == null)
            {
                neprectene = false;
            }
            int pg = page.HasValue ? page.Value : 1;
            MessageDao md = new MessageDao();
            IList<Message> articles = md.getMessageByEmail(phrase, (bool)neprectene , itemsOnPage, (int)pg, out totalPosts);
            ViewBag.Pages = Math.Ceiling((double)totalPosts / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;
            ViewBag.Phrase = phrase;
            if (Request.IsAjaxRequest())
            {
                return PartialView(articles);
            }
            return View(articles);
        }

        public ActionResult Deactivate(int idMessage, bool akt)
        {
            MessageDao md = new MessageDao();
            Message m = md.GetById(idMessage);
            m.Completed = akt;
            md.Update(m);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int idMessage)
        {
            MessageDao md = new MessageDao();
            Message m = md.GetById(idMessage);
            md.Delete(m);
            return RedirectToAction("Index");
        }
    }

}